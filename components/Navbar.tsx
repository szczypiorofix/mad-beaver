import Link from "next/link";

import "../style/navbar.scss";

/**
 * Properties for active Navbar element. In this case - Link
 */
interface IHeaderActiveProps {
  ulEl: string[];
}

const Navbar: React.FunctionComponent<IHeaderActiveProps> = (h: IHeaderActiveProps) => (
  <nav className="navbar">
    <ul>
      <li className={h.ulEl[0]}>
        <Link href="/">
          <a>Home</a>
        </Link>
      </li>
      <li className={h.ulEl[1]}>
        <Link href="/about">
          <a>About</a>
        </Link>
      </li>
    </ul>
  </nav>
);

export default Navbar;

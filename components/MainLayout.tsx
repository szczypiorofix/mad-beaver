import Head from "next/head";
import { Fragment, FunctionComponent } from "react";
import Navbar from "./Navbar";

import "../style/footer.scss";

interface IProps {
  title?: string;
  ulEl: string[];
  children: JSX.Element[] | JSX.Element;
}

const MainLayout: FunctionComponent<IProps> = (props: IProps): JSX.Element => {
  return (
    <Fragment>
      <Head>
        <title>{props.title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="icon" type="image/png" href="/static/icon.png"></link>
      </Head>
      <Navbar ulEl={props.ulEl} />
      {props.children}
      <footer>
        <p className="footer-content">This is footer</p>
      </footer>
    </Fragment>
  );
};

export default MainLayout;

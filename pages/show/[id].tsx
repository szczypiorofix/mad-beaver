import fetch from 'isomorphic-unfetch';
import { useRouter } from 'next/router';
import MainLayout from '../../components/MainLayout';


export default function Post(props: any): JSX.Element {
  const router = useRouter();

  console.log(router.query);
  return (
    <MainLayout title="About" ulEl={["", ""]}>
      <div className="article">
          <div className="title">
            <h1>{router.query.id}</h1>
          </div>
          <div className="content">
            <h1>{props.show.name}</h1>
            <p>{props.show.summary.replace(/<[/]?p>/g, '')}</p>
            <img src={props.show.image.medium} />
          </div>
        </div>
    </MainLayout>
  );
}

Post.getInitialProps = async (context: any) => {
  const { id } = context.query;
  const res = await fetch(`https://api.tvmaze.com/shows/${id}`);
  const show = await res.json();

  console.log(`Fetched show: ${show.name}`);

  return { show };
};

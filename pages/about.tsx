import { Fragment } from "react";
import MainLayout from "../components/MainLayout";

import "../style/main.scss";

export default function About(): JSX.Element {
  return (
    <Fragment>
      <MainLayout title="About" ulEl={["", "active"]}>
        <div className="article">
          <div className="title">
            <h1>ABOUT PAGE</h1>
          </div>
          <div className="content">
            <p>This is just an about page.</p>
          </div>
        </div>
      </MainLayout>
    </Fragment>
  );
}

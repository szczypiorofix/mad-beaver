import fetch from 'isomorphic-unfetch';
import Link from "next/link";
import { Fragment } from "react";
import MainLayout from "../components/MainLayout";

import "../style/main.scss";


Index.getInitialProps = async () => {
  const res = await fetch('https://api.tvmaze.com/search/shows?q=batman');
  const data = await res.json();

  console.log(`Show data fetched. Count: ${data.length}`);

  return {
    shows: data.map( (entry:any) => entry.show)
  };
};

export default function Index(props: any) {
  return (
    <Fragment>
      <MainLayout title="Home" ulEl={["active", ""]}>
        <div className="article">
          <div className="title">
            <h1>MAD BEAVER</h1>
          </div>
          <div className="content">
            <p>What if I told you ...</p>
            <div>
              <h2>Shows:</h2>
              <ul>
                {props.shows.map( (show: any) => (
                  <li key={show.id}>
                    <Link href="/show/[id]" as={`/show/${show.id}`}>
                      <a>{show.name}</a>
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
            <img src="/static/matrix.jpg"></img>
          </div>
        </div>
      </MainLayout>
    </Fragment>
  );
}

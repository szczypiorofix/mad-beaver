const withSass = require("@zeit/next-sass");

module.exports = withSass({
  cssModules: false,
  exportPathMap: function() {
    return {
      "/": { page: "/" },
    };
  },
});
